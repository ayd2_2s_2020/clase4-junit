package com.usac.junit;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PersonTest {
    
    @Test
	public void getterMethods() {
		Person bart = new Person("Bart", "Simpson");
		assertEquals("BartSimpson", bart.getFullName());
		assertEquals("Bart", bart.getFirstName());
		assertEquals("Simpson", bart.getLastName());
    }
    
}
